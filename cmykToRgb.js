
/*
 * Converts an CMYK color value to a RGB value. 
 * Assumes cmyk_c, cmyk_m, cmyk_y, cmyk_k are each between 0 and 100.
 * Returns an array of rgb_r, rgb_g, rgb_b values.
 *
 * @param   Number  rgb_r       The red color value
 * @param   Number  rgb_g       The green color value
 * @param   Number  rgb_b       The blue color value
 * @return  Array           The CMYK color values
 */
 function cmykToRgb(cmyk_c, cmyk_m, cmyk_y, cmyk_k){
	cmyk_c = (cmyk_c / 100);
	cmyk_m = (cmyk_m / 100);
	cmyk_y = (cmyk_y / 100);
	cmyk_k = (cmyk_k / 100);
    
	cmyk_c = cmyk_c * (1 - cmyk_k) + cmyk_k;
	cmyk_m = cmyk_m * (1 - cmyk_k) + cmyk_k;
	cmyk_y = cmyk_y * (1 - cmyk_k) + cmyk_k;
    
	var rgb_r = 1 - cmyk_c;
	var rgb_g = 1 - cmyk_m;
	var rgb_b = 1 - cmyk_y;
    
	rgb_r = Math.round(255 * rgb_r);
	rgb_g = Math.round(255 * rgb_g);
	rgb_b = Math.round(255 * rgb_b);
    
	return [rgb_r, rgb_g, rgb_b]
}