
/*
 * Converts an RGB color value to a CMYK value. 
 * Assumes rgb_r, rgb_g, and rgb_b are each between 0 and 255.
 * Returns an array of cmyk_c,cmyk_m,cmyk_y,cmyk_k values.
 *
 * @param   Number  rgb_r       The red color value
 * @param   Number  rgb_g       The green color value
 * @param   Number  rgb_b       The blue color value
 * @return  Array           The CMYK color values
 */
 function rgb2cmyk (rgb_r,rgb_g,rgb_b) {
	var cmyk_c = 0;
	var cmyk_m = 0;
	var cmyk_y = 0;
	var cmyk_k = 0;
	
	//remove spaces from input RGB values, convert to int
	var rgb_r = parseInt( (''+rgb_r).replace(/\s/g,''),10 ); 
	var rgb_g = parseInt( (''+rgb_g).replace(/\s/g,''),10 ); 
	var rgb_b = parseInt( (''+rgb_b).replace(/\s/g,''),10 ); 
	
	if ( rgb_r==null || rgb_g==null || rgb_b==null || isNaN(rgb_r) || isNaN(rgb_g)|| isNaN(rgb_b) ) {
		alert ('Please enter numeric RGB values!');
		return;
	}
	if (rgb_r<0 || rgb_g<0 || rgb_b<0 || rgb_r>255 || rgb_g>255 || rgb_b>255) {
		alert ('RGB values must be in the range 0 to 255.');
		return;
	}
	
	// BLACK
	if (rgb_r==0 && rgb_g==0 && rgb_b==0) {
		cmyk_k = 1;
		return [0,0,0,1];
	}
	
	cmyk_c = 1 - (rgb_r/255);
	cmyk_m = 1 - (rgb_g/255);
	cmyk_y = 1 - (rgb_b/255);
	
	var minCMY = Math.min(cmyk_c, Math.min(cmyk_m,cmyk_y));
	cmyk_c = (cmyk_c - minCMY) / (1 - minCMY) ;
	cmyk_m = (cmyk_m - minCMY) / (1 - minCMY) ;
	cmyk_y = (cmyk_y - minCMY) / (1 - minCMY) ;
	cmyk_k = minCMY;
	
	return [cmyk_c,cmyk_m,cmyk_y,cmyk_k];
}