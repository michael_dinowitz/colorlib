
/*
 * Converts an RGB color value to an HSL value. 
 * Conversion formula adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes rgb_r, rgb_g, and rgb_b are each between 0 and 255.
 * Returns hsl_h, hsl_s, and hsl_l as 0, 1, or a decimal value between them.
 *
 * @param   Number  rgb_r       The red color value
 * @param   Number  rgb_g       The green color value
 * @param   Number  rgb_b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(rgb_r, rgb_g, rgb_b) {
	rgb_r /= 255, rgb_g /= 255, rgb_b /= 255;
	var max = Math.max(rgb_r, rgb_g, rgb_b), min = Math.min(rgb_r, rgb_g, rgb_b);
	var hsl_h, hsl_s, hsl_l = (max + min) / 2;

	if (max == min) {
		hsl_h = hsl_s = 0;
	} 
	else {
		var d = max - min;
		hsl_s = hsl_l > 0.5 ? d / (2 - max - min) : d / (max + min);

		switch (max) {
			case rgb_r: hsl_h = (rgb_g - rgb_b) / d + (rgb_g < rgb_b ? 6 : 0); break;
			case rgb_g: hsl_h = (rgb_b - rgb_r) / d + 2; break;
			case rgb_b: hsl_h = (rgb_r - rgb_g) / d + 4; break;
		}
			
		hsl_h /= 6;
	}
		
	return [hsl_h, hsl_s, hsl_l];
}