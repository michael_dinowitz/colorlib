// http://stackoverflow.com/questions/8022885/rgb-to-hsv-color-in-javascript
// console.log( rgb2hsv(60, 120, 180) );

function rgbToHsv () {
	var rr, gg, bb,
	r = arguments[0] / 255,
	g = arguments[1] / 255,
	b = arguments[2] / 255,
	h, s,
	v = Math.max(r, g, b),
	diff = v - Math.min(r, g, b),
	diffc = function(c) {
		return (v - c) / 6 / diff + 1 / 2;
	};
	
	if (diff == 0) {
		h = s = 0;
	} else {
		s = diff / v;
		rr = diffc(r);
		gg = diffc(g);
		bb = diffc(b);
	
		if (r === v) {
			h = bb - gg;
		} else if (g === v) {
			h = (1 / 3) + rr - bb;
		} else if (b === v) {
			h = (2 / 3) + gg - rr;
		}
	
		if (h < 0) {
			h += 1;
		} else if (h > 1) {
			h -= 1;
		}
	}
	
	return {
		h: Math.round(h * 360),
		s: Math.round(s * 100),
		v: Math.round(v * 100)
	};
}

// RGB-to-HSV Color Converter	http://www.javascripter.net/faq/rgb2hsv.htm
function rgbToHsv (rgb_r, rgb_g, rgb_b) {
	var hsv_h = 0;
	var hsv_s = 0;
	var hsv_v = 0;
	
	//remove spaces from input RGB values, convert to int
	var rgb_r = parseInt( (''+rgb_r).replace(/\s/g,''),10 ); 
	var rgb_g = parseInt( (''+rgb_g).replace(/\s/g,''),10 ); 
	var rgb_b = parseInt( (''+rgb_b).replace(/\s/g,''),10 ); 
	
	if ( rgb_r==null || rgb_g==null || rgb_b==null ||
	isNaN(rgb_r) || isNaN(rgb_g)|| isNaN(rgb_b) ) {
		alert ('Please enter numeric RGB values!');
		return;
	}
	
	if (rgb_r<0 || rgb_g<0 || rgb_b<0 || rgb_r>255 || rgb_g>255 || rgb_b>255) {
		alert ('RGB values must be in the range 0 to 255.');
		return;
	}
	
	rgb_r=rgb_r/255; rgb_g=rgb_g/255; rgb_b=rgb_b/255;
	var minRGB = Math.min(rgb_r,Math.min(rgb_g,rgb_b));
	var maxRGB = Math.max(rgb_r,Math.max(rgb_g,rgb_b));
	
	// Black-gray-white
	if (minRGB==maxRGB) {
		hsv_v = minRGB;
		return [0,0,hsv_v];
	}
	
	// Colors other than black-gray-white:
	var d = (rgb_r==minRGB) ? rgb_g-rgb_b : ((b==minRGB) ? rgb_r-rgb_g : rgb_b-rgb_r);
	var h = (rgb_r==minRGB) ? 3 : ((rgb_b==minRGB) ? 1 : 5);
	hsv_h = 60*(h - d/(maxRGB - minRGB));
	hsv_s = (maxRGB - minRGB)/maxRGB;
	hsv_v = maxRGB;
	
	return [hsv_h, hsv_s, hsv_v];
}