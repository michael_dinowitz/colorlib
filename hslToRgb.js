
/*
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes hsl_h, hsl_s, and hsl_l are contained in the set [0, 1] and
 * returns rgb_r, rgb_g, and rgb_b in the set [0, 255].
 *
 * @param   {number}  hsl_h       The hue
 * @param   {number}  hsl_s       The saturation
 * @param   {number}  hsl_l       The lightness
 * @return  {Array}           The RGB representation
 */
function hslToRgb(hsl_h, hsl_s, hsl_l){
    var rgb_r, rgb_g, rgb_b;

    if(hsl_s == 0){
        rgb_r = rgb_g = rgb_b = hsl_l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = hsl_l < 0.5 ? hsl_l * (1 + hsl_s) : hsl_l + hsl_s - l * hsl_s;
        var p = 2 * hsl_l - q;
        rgb_r = hue2rgb(p, q, hsl_h + 1/3);
        rgb_g = hue2rgb(p, q, hsl_h);
        rgb_b = hue2rgb(p, q, hsl_h - 1/3);
    }

    return [Math.round(rgb_r * 255), Math.round(rgb_g * 255), Math.round(rgb_b * 255)];
}