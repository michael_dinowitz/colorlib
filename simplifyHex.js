/*
* Simplifies a hexidecimal color value
* Assumes a hexidecimal color value of 6 hexidecimal characters
* Returns a 6 character hexidecimal color value.
*
* @param	String	hex	a 6 character hexidecimal color value
* @return	String	a 6 character hexidecimal color value
*/

function simplifyHex (hex) {
	if (hex.length != 6) {
		alert ('Invalid length of the input hex value!');   
		return; 
	}
	if (/[0-9a-f]{6}/i.test(hex) != true) {
		alert ('Invalid digits in the input hex value!');
		return; 
	}

	return hex[0] + hex[0] + hex[2] + hex[2] + hex[4] + hex[4];
}