
/*
 * Converts an RGB color value to an Hex value. 
 * Assumes rgb_r, rgb_g, and rgb_b are each between 0 and 255.
 * Returns a 6 character hexidecimal color value.
 *
 * @param   Number  rgb_r       The red color value
 * @param   Number  rgb_g       The green color value
 * @param   Number  rgb_b       The blue color value
 * @return  String           The hexidecimal color value
 */
function rgbToHex(rgb_r,rgb_g,rgb_b) {
	return toHex(rgb_r)+toHex(rgb_g)+toHex(rgb_b)
}
function toHex(n) {
	n = parseInt(n,10);
	if (isNaN(n)) 
		return "00";
	n = Math.max(0,Math.min(n,255));

	return "0123456789ABCDEF".charAt((n-n%16)/16)+ "0123456789ABCDEF".charAt(n%16);
}